# IPL Data Project I

**Download the data from:** https://www.kaggle.com/manasgarg/ipl

There should be 2 files:

- deliveries.csv
- matches.csv

In this data assignment you will transform raw data of IPL to calculate the following stats:

1. Number of matches played per year for all the years in IPL.
2. Number of matches won per team per year in IPL.
3. Extra runs conceded per team in the year 2016
4. Top 10 economical bowlers in the year 2015

Implement the 4 functions, one for each task. Use the results of the functions to dump JSON files in the output folder

## Instructions:

- Create a new repo with name *<your_first_name>_IPL_Project* in Gitlab, before starting implementation of the solution
- Make sure to follow proper Git practices
- Before submission, make sure that all the points in the below checklist are covered:
  - Git commits
  - Directory structure
  - package.json - dependencies, devDependencies
  - .gitignore file
  - Proper/Intuitive Variable names
  - Separate module for functions

## Directory structure:

- src/
  - server/
    - ipl.js
    - index.js
  - public/
    - output
      - matchesPerYear.json
      - ...
    - index.html
    - style.css
  - data/
    - matches.csv
    - deliveries.csv
- package.json
- package-lock.json
- .gitignore
