/*

In this data assignment you will transform raw data of IPL to calculate the following stats:

1. Number of matches played per year for all the years in IPL.
2. Number of matches won per team per year in IPL.
3. Extra runs conceded per team in the year 2016
4. Top 10 economical bowlers in the year 2015

Implement the 4 functions, one for each task. Use the results of the functions to dump JSON files in the output folder

*/

const fs = require('fs');
const csvtojsonV1 = require("csvtojson");

/* Modules */
const extraRunsPerTeamForGivenYear = require('./modules/extraRunsPerTeamForGivenYear.js');
const matchesPerYear = require('./modules/matchesPerYear.js');
const matchesWonPerTeamPerYear = require('./modules/matchesWonPerTeamPerYear.js');
const top10EconomicBowlersForGivenYear = require('./modules/top10EconomicBowlersForGivenYear.js');

/* Output file paths */
const matchesPerYearJsonFile = '../public/output/matchesPerYear.json';
const matchesWonPerTeamPerYearJsonFile = '../public/output/matchesWonPerTeamPerYear.json'; 
const extraRunsPerTeamForGivenYearJsonFile = '../public/output/extraRunsPerTeamForGivenYear.json';
const top10EconomicBowlersForGivenYearJsonFile = '../public/output/top10EconomicBowlersForGivenYear.json';

/* Match Data  */
const csvFileMatches='../data/matches.csv'

/* Delivery Data  */
const csvFileDeliveries='../data/deliveries.csv'

csvtojsonV1()
  .fromFile(csvFileMatches)
  .then((matchData)=>{
    writeToFile(matchesPerYear(matchData), matchesPerYearJsonFile); 
    writeToFile(matchesWonPerTeamPerYear(matchData), matchesWonPerTeamPerYearJsonFile); 

    csvtojsonV1()
      .fromFile(csvFileDeliveries)
      .then((deliveryData)=>{
          writeToFile(extraRunsPerTeamForGivenYear(matchData, deliveryData, '2016'), extraRunsPerTeamForGivenYearJsonFile);
          writeToFile(top10EconomicBowlersForGivenYear(matchData, deliveryData, '2015'), top10EconomicBowlersForGivenYearJsonFile);
      });
  });


function writeToFile(data, filePath) {
  data = JSON.stringify(data, null, 2); 
  fs.writeFile(filePath, data, () => {});
}

/**
 * Delivery
 * {
    "match_id": "1",
    "inning": "1",
    "batting_team": "Sunrisers Hyderabad",
    "bowling_team": "Royal Challengers Bangalore",
    "over": "1",
    "ball": "3",
    "batsman": "DA Warner",
    "non_striker": "S Dhawan",
    "bowler": "TS Mills",
    "is_super_over": "0",
    "wide_runs": "0",
    "bye_runs": "0",
    "legbye_runs": "0",
    "noball_runs": "0",
    "penalty_runs": "0",
    "batsman_runs": "4",
    "extra_runs": "0",
    "total_runs": "4",
    "player_dismissed": "",
    "dismissal_kind": "",
    "fielder": ""
  },
  
  Matches
  {
    "id": "1",
    "season": "2017",
    "city": "Hyderabad",
    "date": "2017-04-05",
    "team1": "Sunrisers Hyderabad",
    "team2": "Royal Challengers Bangalore",
    "toss_winner": "Royal Challengers Bangalore",
    "toss_decision": "field",
    "result": "normal",
    "dl_applied": "0",
    "winner": "Sunrisers Hyderabad",
    "win_by_runs": "35",
    "win_by_wickets": "0",
    "player_of_match": "Yuvraj Singh",
    "venue": "Rajiv Gandhi International Stadium, Uppal",
    "umpire1": "AY Dandekar",
    "umpire2": "NJ Llong",
    "umpire3": ""
  },

 */