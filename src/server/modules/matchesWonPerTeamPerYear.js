
/* Number of matches won per team per year in IPL. */
function matchesWonPerTeamPerYear(matchData) {
    let statObj = {};
    for(let match of matchData) {
        if(statObj[match.season] === undefined) {
            statObj[match.season] = {}; 
        }
        if(statObj[match.season][match.winner] === undefined) {
            statObj[match.season][match.winner] = 0; 
        }
        statObj[match.season][match.winner]++;
    }
    return statObj;
}

module.exports = matchesWonPerTeamPerYear;