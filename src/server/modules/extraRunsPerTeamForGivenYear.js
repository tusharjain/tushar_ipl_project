
/* Extra runs conceded per team in a year */
function extraRunsPerTeamForGivenYear(matchData, deliveryData, year) {
    let idArray = []; // Collect IDs of matches played in the year
    for(let match of matchData) {
        if(match.season === year) {
            idArray.push(match.id); 
        }
    }

    let statObj = {}; // store extra runs for each team    
    for(let delivery of deliveryData) {
        if(idArray.includes(delivery.match_id)) {
            if(statObj[delivery.bowling_team] === undefined) {
                statObj[delivery.bowling_team] = 0;
            }
            statObj[delivery.bowling_team] += +delivery.extra_runs; 
        }
    }
    return statObj; 
}

module.exports = extraRunsPerTeamForGivenYear;