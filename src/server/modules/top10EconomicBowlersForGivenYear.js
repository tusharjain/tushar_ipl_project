const fs = require('fs');

/* Top 10 economical bowlers in a year */
function top10EconomicBowlersForGivenYear(matchData, deliveryData, year) {
    let idArray = []; // Collect IDs of matches played in the year
    for(let match of matchData) {
        if(match.season === year) {
            idArray.push(match.id); 
        }
    }

    let statObj = {}; // store bowling data for each bowler
    for(let delivery of deliveryData) {
        if(idArray.includes(delivery.match_id)) {
            if(statObj[delivery.bowler] === undefined) {
                statObj[delivery.bowler] = {
                                              runs: 0,
                                              deliveries: 0
                                           }; 
            }
            statObj[delivery.bowler]['runs'] += +delivery.total_runs;
            statObj[delivery.bowler]['deliveries'] += 1;
        }
    }
    

    for(let bowler in statObj) { 
         statObj[bowler]['overs'] = Number.parseFloat(statObj[bowler]['deliveries'] / 6).toFixed(3); // convert No. of deliveries to No. of overs
         statObj[bowler]['economy'] = Number.parseFloat(statObj[bowler]['runs'] / statObj[bowler]['overs']).toFixed(3); // find economy rate
    }
    
    let top10EconomicBowlers = Object.keys(statObj)
                                    .sort((a, b) => {
                                    return +statObj[a]['economy'] - +statObj[b]['economy'];
                                    })
                                    .slice(0, 10)
                                    .reduce((top10List, bowler) => {
                                        top10List[bowler] = statObj[bowler]['economy'];
                                        return top10List;
                                    }, {});

    return top10EconomicBowlers;                                     
}

module.exports = top10EconomicBowlersForGivenYear;