
/* Number of matches played per year for all the years in IPL. */
function matchesPerYear(matchData) {
    let statObj = {}; 
    for(let match of matchData) {
        if(statObj[match.season] === undefined) {
            statObj[match.season] = 0; 
        }
        statObj[match.season]++;
    }
    return statObj; 
}

module.exports = matchesPerYear;